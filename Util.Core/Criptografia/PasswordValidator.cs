﻿using System.Collections.Generic;
using System.Linq;

namespace LabnetComun.Criptografia
{
    /// <summary>
    /// Valida que una contraseña cumple con unos requisitos determinados
    /// </summary>
    public class PasswordValidator
    {
        private char[] digitos = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        private char[] mayusculas = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Á', 'É', 'Í', 'Ó', 'Ú' };
        private char[] minusculas = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'á', 'é', 'í', 'ó', 'ú' };

        /// <summary>
        /// Longitud que debe cumplir la contraseña
        /// </summary>
        public int LongitudRequerida { get; private set; }

        /// <summary>
        /// Determina si se requiere un dígito del 0 al 9 en la contraseña
        /// </summary>
        public bool RequiereDigito { get; private set; }

        /// <summary>
        /// Determina si se requiere una letra mayúscula para la contraseña
        /// </summary>
        public bool RequiereMayuscula { get; private set; }

        /// <summary>
        /// Determina si se requiere una letra minúscula para la contraseña
        /// </summary>
        public bool RequiereMinuscula { get; private set; }

        /// <summary>
        /// Inicializa el validador
        /// </summary>
        /// <param name="longitudRequerida"></param>
        /// <param name="requiereDigito"></param>
        /// <param name="requiereMayuscula"></param>
        /// <param name="requiereMinuscula"></param>
        public PasswordValidator(int longitudRequerida, bool requiereDigito, 
            bool requiereMayuscula, bool requiereMinuscula)
        {
            LongitudRequerida = longitudRequerida;
            RequiereDigito = requiereDigito;
            RequiereMayuscula = requiereMayuscula;
            RequiereMinuscula = requiereMinuscula;
        }

        /// <summary>
        /// Si la contraseña cumple con los requerimientos
        /// </summary>
        /// <param name="pass">contraseña a validar</param>
        /// <returns></returns>
        public bool Cumple(string pass) => !Mensaje(pass).Any();

        /// <summary>
        /// Lista de advertencias por lo que la contraseña no cumple los requerimientos 
        /// </summary>
        /// <param name="pass">contraseña a validar</param>
        /// <returns></returns>
        public IEnumerable<string> Mensaje(string pass)
        {
            if (!CumpleLongitud(pass)) yield return $"La contraseña debe tener {LongitudRequerida} caracteres de longitud como mínimo";
            if (!CumpleDigito(pass)) yield return "La contraseña debe contener una letra mayúscula por lo menos";
            if (!CumpleMayuscula(pass)) yield return "La contraseña debe contener una letra mayúscula por lo menos";
            if (!CumpleMinuscula(pass)) yield return "La contraseña debe contener una letra mayúscula por lo menos";
        }

        private bool CumpleLongitud(string pass) => pass.Length > LongitudRequerida;
        private bool CumpleDigito(string pass) => !RequiereDigito ? true : pass.Any(x => digitos.Contains(x));
        private bool CumpleMayuscula(string pass) => !RequiereMayuscula ? true : pass.Any(x => mayusculas.Contains(x));
        private bool CumpleMinuscula(string pass) => !RequiereMinuscula ? true : pass.Any(x => minusculas.Contains(x));
    };
}
