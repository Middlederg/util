﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace LabnetComun.Criptografia
{
    /// <summary>
    /// Permite tratar con contraseñas
    /// </summary>
    public sealed class Securizador
    {
        /// <summary>
        /// Tamaño del salt
        /// </summary>
        private const int saltSize = 16;

        /// <summary>
        /// Tamaño del hash
        /// </summary>
        private const int hashSize = 20;

        /// <summary>
        /// Iteraciones
        /// </summary>
        private const int iterations = 10000;

        /// <summary>
        /// Creates a hash from a password
        /// </summary>
        /// <param name="password">the password</param>
        /// <param name="iterations">number of iterations</param>
        /// <returns>the hash</returns>
        public static string Hash(string password, int iterations = 10000)
        {
            //create salt
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[saltSize]);

            //create hash
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, iterations);
            var hash = pbkdf2.GetBytes(hashSize);

            //combine salt and hash
            var hashBytes = new byte[saltSize + hashSize];
            Array.Copy(salt, 0, hashBytes, 0, saltSize);
            Array.Copy(hash, 0, hashBytes, saltSize, hashSize);

            //convert to base64 string
            return Convert.ToBase64String(hashBytes);
        }

        /// <summary>
        /// Verify a password against a hash
        /// </summary>
        /// <param name="password">the password</param>
        /// <param name="hashedPassword">the hash</param>
        /// <returns>could be verified?</returns>
        public static bool Verify(string password, string hashedPassword)
        {
            //get hashbytes
            var hashBytes = Convert.FromBase64String(hashedPassword);

            //get salt
            var salt = new byte[saltSize];
            Array.Copy(hashBytes, 0, salt, 0, saltSize);

            //create hash with given salt
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, iterations);
            byte[] hash = pbkdf2.GetBytes(hashSize);

            //get result
            for (var i = 0; i < hashSize; i++)
            {
                if (hashBytes[i + saltSize] != hash[i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}
