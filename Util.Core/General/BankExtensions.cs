﻿using System;
using System.Linq;

namespace LabnetComun.General
{
    /// <summary>
    /// Extensiones de ayuda para cálculos relacionados con bancos
    /// </summary>
    public static class BankExtensions
    {
        /// <summary>
        /// Calcula el dígito de control (2 cifras) a partir del numero de cuenta
        /// </summary>
        /// /// <param name="entidad">Cadena con número de 4 cifras</param>
        /// /// <param name="centro">Cadena con número de 4 cifras</param>
        /// <param name="cuenta">Cadena con numero de cuenta (10 cifras)</param>
        /// <returns>Cadena con el dígito de control</returns>
        public static string DigitoControl(string entidad, string centro, string cuenta)
        {
            if (string.IsNullOrWhiteSpace(entidad) || entidad.Length > 4)
                throw new ArgumentNullException("Entidad no válida");
            if (string.IsNullOrWhiteSpace(centro) || entidad.Length > 4)
                throw new ArgumentNullException("Centro no válido");
            if (string.IsNullOrWhiteSpace(cuenta) || entidad.Length > 10)
                throw new ArgumentNullException("Cuenta no válida");
            if (!(Int32.TryParse(entidad, out int num) && Int32.TryParse(centro, out num) && Int32.TryParse(centro, out num)))
                throw new ArgumentException("Numero de cuenta, centro o entidad no sn cadenas numéricas");

            entidad = entidad.ColocarCerosIzquierda(4);
            centro = centro.ColocarCerosIzquierda(4);
            cuenta = cuenta.ColocarCerosIzquierda(10);

            return CalculaDigito("00" + entidad + centro).ToString() + CalculaDigito(cuenta).ToString();
        }

        /// <summary>
        /// Calcula el dígito de control
        /// </summary>
        /// <param name="numeros">cadena de longitud 10</param>
        /// <returns></returns>
        private static int CalculaDigito(string numeros)
        {
            if (string.IsNullOrWhiteSpace(numeros) || numeros.Length != 10)
                throw new ArgumentOutOfRangeException("El numero debe ser una cadena de longitud 10");
            int[] factores = { 1, 2, 4, 8, 5, 10, 9, 7, 3, 6 };
            int result = 0;
            for (int i = 0; i < factores.Length; i++)
                result += Int32.Parse((numeros).ElementAt(i).ToString()) * factores[i];
            int dig = 11 - (result % 11);
            if (dig == 10) dig = 1;
            if (dig == 11) dig = 0;
            return dig;
        }
    }
}
