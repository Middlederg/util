﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    /// <summary>
    /// Métodos de utilidad para cadenas de texto
    /// </summary>
    public static class Cadenas
    {
        /// <summary>
        /// Constante que contiene números
        /// </summary>
        public const string NUMEROS = "0123456789";

        /// <summary>
        /// Constante que contiene letras minúsculas del abecedario
        /// </summary>
        public const string MINUSCULAS = "abcdefghijklmnñopqrstuvwxyzáéíóú";

        /// <summary>
        /// Constante que contiene letras mayúsculas del abecedario
        /// </summary>
        public const string MAYUSCULAS = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZÁÉÍÓÚ";

        private static Dictionary<char, char> AcentosMinusculas = new Dictionary<char, char>()
        {
            { 'á', 'a' },
            { 'é', 'e' },
            { 'í', 'i' },
            { 'ó', 'o' },
            { 'ú', 'u' }
        };
        private static Dictionary<char, char> AcentosMayusculas = new Dictionary<char, char>()
        {
            { 'Á', 'A' },
            { 'É', 'E' },
            { 'Í', 'I' },
            { 'Ó', 'O' },
            { 'Ú', 'U' }
        };

        /// <summary>
        /// Devuelve el texto cortado con la longitud especificada
        /// </summary>
        /// <param name="texto">Texto a cortar</param>
        /// <param name="limite">Número de caracteres máximo que cortará</param>
        /// <returns></returns>
        public static string Limitar(this string texto, int limite)
        {
            if (texto == null)
                return "";
            return texto.Trim().Length > limite ? texto.Trim().Substring(0, limite) : texto.Trim();
        }

        /// <summary>
        /// Devuelve el texto cortado con la longitud especificada, y pone puntos suspensivos al final
        /// </summary>
        /// <param name="texto">Texto a cortar</param>
        /// <param name="limite">Número de caracteres máximo que cortará</param>
        /// <param name="unEspacio">Pone un espacio entre el texto y los puntos suspensivos</param>
        /// <returns></returns>
        public static string PuntosSuspensivos(this string texto, int limite, bool unEspacio = false)
        {
            if (texto == null)
                return "";
            return texto.Trim().Length > limite ? texto.Trim().Substring(0, limite) + (unEspacio ? " " : "") + "..." : texto.Trim();
        }

        /// <summary>
        /// Quita el ultimo caracter de una cadena
        /// </summary>
        /// <param name="cadena">Cadena a cortar</param>
        /// <returns></returns>
        public static string QuitarUltimoCaracter(this string cadena)
        {
            if (cadena == null || cadena.Length < 1)
                return "";
            return cadena.Substring(0, cadena.Length - 1);
        }

        /// <summary>
        /// Quita los últimos n caracteres de una cadena
        /// </summary>
        /// <param name="cadena">Cadena a cortar</param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static string QuitarUltimos(this string cadena, int n)
        {
            if (cadena == null || n > cadena.Length)
                return "";
            return cadena.Substring(0, cadena.Length - n);
        }

        /// <summary>
        /// Quita el primer caracter de una cadena
        /// </summary>
        /// <param name="cadena">Cadena a cortar</param>
        /// <returns></returns>
        public static string QuitarPrimerCaracter(this string cadena)
        {
            if (string.IsNullOrEmpty(cadena))
                return "";
            return cadena.Substring(1);
        }

        /// <summary>
        /// Quita los primeros n caracteres de una cadena
        /// </summary>
        /// <param name="cadena">Cadena a cortar</param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static string QuitarPrimeros(this string cadena, int n)
        {
            if (cadena == null || n > cadena.Length)
                return "";
            return cadena.Substring(n);
        }

        /// <summary>
        /// Quita los acentos de un string
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        public static string QuitarAcentos(this string entrada)
        {
            StringBuilder s = new StringBuilder(entrada);
            foreach (char k in AcentosMayusculas.Keys)
                s.Replace(k, AcentosMayusculas[k]);
            foreach (char k in AcentosMinusculas.Keys)
                s.Replace(k, AcentosMinusculas[k]);
            return s.ToString();
        }

        /// <summary>
        /// Limpia texto y lo deja solo con letras y números
        /// </summary>
        /// <param name="textox">texto a limpiar</param>
        /// <param name="dejarEspacios">Para dejar los espacios en el texto final</param>
        /// <returns>texto limpiado</returns>
        public static string LimpiarTexto(this string textox, bool dejarEspacios = false)
        {
            if (string.IsNullOrWhiteSpace(textox)) return string.Empty;
            return textox.Where(c => NUMEROS.Contains(c) ||
                                        MINUSCULAS.Contains(c) ||
                                        MAYUSCULAS.Contains(c) ||
                                        (dejarEspacios && char.IsWhiteSpace(c)))
            .ToString();
        }

        /// <summary>
        /// Comprueba que un texto contiene otro texto.
        /// </summary>
        /// <param name="texto">texto principal</param>
        /// <param name="buscar">texto a buscar %texto% </param>
        /// <param name="caseSensitive"> Si se quiere que distinga mayúsculas de minúsculas, acentos, etc. Por defecto "pH in situ" contiene "PH" </param>
        /// <returns></returns>
        public static bool ContieneTexto(this string texto, string buscar, bool caseSensitive = false)
        {
            if (texto == null)
                return false;

            if (!caseSensitive)
            {
                texto = texto.QuitarAcentos().ToUpper();
                buscar = buscar == null ? "" : buscar.QuitarAcentos().ToUpper();
            }
            return texto.Trim().Contains(buscar.Trim());
        }

        /// <summary>
        /// Pone la primera letra de cada palabra en mayúsculas
        /// </summary>
        /// <param name="cadena"></param>
        /// <param name="espacios"> elimina los espacios o no</param>
        /// <returns></returns>
        public static string Capitalize(this string cadena, bool espacios = false)
        {
            try
            {
                StringBuilder resultado = new StringBuilder();
                int indexFrase = 0;
                foreach (char letrax in cadena)
                {
                    if (indexFrase == 0)
                        resultado.Append(char.ToUpper(letrax));
                    else
                    {
                        if (char.IsWhiteSpace(letrax))
                        {
                            indexFrase = 0;
                            if (!espacios)
                                resultado.Append(' ');
                        }
                        else
                            resultado.Append(letrax);
                    }
                    if (indexFrase != 0)
                        indexFrase++;
                }
                return resultado.ToString();
            }
            catch { return cadena; } //si falla devuelve la frase original
        }

        /// <summary>
        /// Pone un espacio entre cada letra mayúscula, y la primera letra de la frase en mayúscula
        /// </summary>
        /// <param name="frase"></param>
        /// <returns></returns>
        public static string PonEspacios(this string frase)
        {
            StringBuilder final = new StringBuilder();
            foreach (char s in frase)
            {
                if (Char.IsUpper(s))
                    final.Append(" ");
                final.Append(s.ToString().ToLower());
            }
            return final[1].ToString().ToUpper() + final.ToString().Substring(2, final.Length);
        }

        /// <summary>
        /// Devuelve letra DNI
        /// </summary>
        /// <param name="numeroDni"></param>
        /// <returns></returns>
        public static char GetLetraDNI(this string numeroDni)
        {
            if (Int32.TryParse(numeroDni, out int dni))
            {
                if (dni > 0 && dni < 99999999)
                {
                    switch (dni % 23)//obtencion del resto de la division por 23
                    {//en funcion del resto, asigna letra
                        case 0: return 'T';
                        case 1: return 'R';
                        case 2: return 'W';
                        case 3: return 'A';
                        case 4: return 'G';
                        case 5: return 'M';
                        case 6: return 'Y';
                        case 7: return 'F';
                        case 8: return 'P';
                        case 9: return 'D';
                        case 10: return 'X';
                        case 11: return 'B';
                        case 12: return 'N';
                        case 13: return 'J';
                        case 14: return 'Z';
                        case 15: return 'S';
                        case 16: return 'Q';
                        case 17: return 'V';
                        case 18: return 'H';
                        case 19: return 'L';
                        case 20: return 'C';
                        case 21: return 'K';
                        case 22: return 'E';
                        case 23: return 'U';
                        default: return '-'; 
                    }
                }
                else
                {//si valor no valido...
                    throw new ArgumentException("El DNI debe ser un número de 8 cifras");
                }
            }
            else
            {
                throw new ArgumentException("La cadena pasada como parámetro no es número de DNI");
            }
        }

        /// <summary>
        /// Repite x veces el la cadena pasada como parámetro
        /// </summary>
        /// <param name="cadena"></param>
        /// <param name="numVeces">numero de veces que se repite</param>
        /// <returns></returns>
        public static string EscribirNumVeces(this string cadena, int numVeces)
        {
            StringBuilder res = new StringBuilder();
            foreach (var x in Enumerable.Range(0, numVeces))
                res.Append(cadena);
            return res.ToString();
        }

        /// <summary>
        /// Extrae un número entero de un string a45tG643 -> 45643
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        public static int ExtraerNumero(string texto)
        {
            StringBuilder res = new StringBuilder();
            foreach (char c in texto)
            {
                if (Int32.TryParse(c.ToString(), out int num))
                    res.Append(c);
            }
            return Int32.Parse(res.ToString());
        }

        /// <summary>
        /// Pone ceros a la izquierda a la cadena, para que alcance la longitud pasada por parámetro
        /// </summary>
        /// <param name="numeros">Cadena sobre la que se actua</param>
        /// <param name="longitudCadena">Longitud que alcanzará la cadena con ceros a la izquierda</param>
        /// <returns></returns>
        public static string ColocarCerosIzquierda(this string numeros, int longitudCadena)
        {
            if (longitudCadena > numeros.Length)
            {
                int veces = longitudCadena - numeros.Length;
                for (int i = 0; i < veces; i++)
                    numeros = "0" + numeros;
            }
            return numeros;
        }

        /// <summary>
        /// Determines whether it is a valid URL.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if [is valid URL] [the specified text]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidUrl(this string text)
        {
            System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex(@"http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?");
            return rx.IsMatch(text);
        }

        /// <summary>
        /// Determines whether it is a valid email address
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if [is valid email address] [the specified s]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidEmailAddress(this string email)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
            return regex.IsMatch(email);
        }
    }
}
