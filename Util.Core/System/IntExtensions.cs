﻿using System.Text;

namespace System
{
    /// <summary>
    /// Métodos para números enteros
    /// </summary>
    public static class IntExtensions
    {
        /// <summary>
        /// Pone separador de miles a número
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static string ToFormatString(this int num)
        {
            StringBuilder res = new StringBuilder();
            int pos = num.ToString().Length;
            foreach (char c in num.ToString())
            {
                res.Append(c);
                pos--;
                if (pos % 3 == 0 && pos != 0)
                    res.Append(".");
            }
            return res.ToString();
        }
    }
}
