﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace System
{
    /// <summary>
    /// Métodos de ayuda para clases
    /// </summary>
    public static class ClassExtensions
    {
        /// <summary>
        /// Recupera el valor Description de una propiedad de una clase
        /// Si no hay devuelve el nombre de la propiedad de la clase
        /// </summary>
        /// <typeparam name="T">clase</typeparam>
        /// <param name="propertyName">nombre de la propiedad (generalemnte con nameof())</param>
        /// <returns></returns>
        public static string Descripcion<T>(string propertyName) where T : class
        {
            var prop = typeof(T).GetProperty(propertyName);

            var customAts = prop.GetCustomAttributes(typeof(DescriptionAttribute), true);
            if (customAts.FirstOrDefault() is DescriptionAttribute atributo) return atributo.Description;

            return prop.Name;
        }

        /// <summary>
        /// Recupera el valor Description de una propiedad de una clase
        /// Si no hay devuelve el nombre de la propiedad de la clase
        /// </summary>
        /// <param name="prop">Propiedad</param>
        /// <returns></returns>
        public static string Descripcion(this PropertyInfo prop)
        {
            var customAts = prop.GetCustomAttributes(typeof(DescriptionAttribute), true);
            if (customAts.FirstOrDefault() is DescriptionAttribute atributo) return atributo.Description;

            return prop.Name;
        }


        /// <summary>
        /// Recupera el valor Display de una propiedad de una clase
        /// Si no hay devuelve el nombre de la propiedad de la clase
        /// </summary>
        /// <typeparam name="T">clase</typeparam>
        /// <param name="propertyName">nombre de la propiedad (generalemnte con nameof())</param>
        /// <returns></returns>
        public static string DisplayName<T>(string propertyName) where T : class
        {
            var prop = typeof(T).GetProperty(propertyName);

            var customAts = prop.GetCustomAttributes(typeof(DisplayAttribute), true);
            if (customAts.FirstOrDefault() is DisplayAttribute atributo) return atributo.Name;

            return prop.Name;
        }

        /// <summary>
        /// Recupera el valor Display de una propiedad de una clase
        /// Si no hay devuelve el nombre de la propiedad de la clase
        /// </summary>
        /// <param name="prop">Propiedad</param>
        /// <returns></returns>
        public static string DisplayName(this PropertyInfo prop)
        {
            var customAts = prop.GetCustomAttributes(typeof(DisplayAttribute), true);
            if (customAts.FirstOrDefault() is DisplayAttribute atributo) return atributo.Name;

            return prop.Name;
        }
    }
}
