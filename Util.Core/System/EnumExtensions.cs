﻿using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace System
{
    /// <summary>
    /// Métodos de ayuda para enumerados
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Devuelve lista de objetos de tipo Enum con posibles valores de una enumeración
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>Lista de T</returns>
        public static IEnumerable<T> GetEnumList<T>() where T : Enum 
            => new List<T>((T[])Enum.GetValues(typeof(T)));

        /// <summary>
        /// Devuelve lista de strings con posibles valores de una enumeración
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<string> GetEnumListStrings<T>() where T : Enum
        {
            foreach (T item in Enum.GetValues(typeof(T)))
                yield return item.ToString();
        }

        /// <summary>
        /// Devuelve lista de descripciones con posibles valores de una enumeración
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<string> GetEnumListDescripcion<T>() where T : Enum
        {
            foreach (T item in Enum.GetValues(typeof(T)))
                yield return item.Descripcion();
        }

        /// <summary>
        /// Devuelve lista de displayValue con posibles valores de una enumeración
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<string> GetEnumListDisplay<T>() where T : Enum
        {
            foreach (T item in Enum.GetValues(typeof(T)))
                yield return item.DisplayName();
        }

        /// <summary>
        /// Devuelve lista con posibles valores, texto, display y descripcion de una enumeración
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<(T value, string text, string description, string display)> GetEnumListDictionary<T>() where T : Enum
        {
            foreach (T item in Enum.GetValues(typeof(T)))
                yield return (item, item.ToString(), item.Descripcion(), item.DisplayName());
        }

        /// <summary>
        /// Recupera el valor Description de una enumeración
        /// Si no hay devuelve ToString() de la enumeración
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string Descripcion<T>(this T source) where T : Enum
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }

        /// <summary>
        /// Recupera el valor DisplayName de una enumeración.
        /// Si no hay devuelve ToString() de la enumeración
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string DisplayName<T>(this T source) where T : Enum
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DisplayAttribute[] attributes = (DisplayAttribute[])fi.GetCustomAttributes(
                typeof(DisplayAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Name;
            else return source.ToString();
        }

        /// <summary>
        /// Devuelve si el tipo es una enumeración nullable
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static bool IsNullableEnum(this Type t)
        {
            Type u = Nullable.GetUnderlyingType(t);
            return (u != null) && u.IsEnum;
        }
    }
}
