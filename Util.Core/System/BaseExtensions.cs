﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace System
{
    /// <summary>
    /// Métodos de utilidad básicos
    /// </summary>
    public static class BaseExtensions
    {
        /// <summary>
        /// Determina si un valor comparable está entre dos valores o es igual a uno de ellos
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">Valor a comparar</param>
        /// <param name="min">Valor mínimo</param>
        /// <param name="max">Valor máximo</param>
        /// <returns></returns>
        public static bool Between<T>(this T @value, T min, T max) where T : IComparable<T>
        {
            return @value.CompareTo(min) >= 0 && @value.CompareTo(max) <= 0;
        }
    }
}
