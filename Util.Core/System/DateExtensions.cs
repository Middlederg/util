﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    /// <summary>
    /// Métodos de utilidad para fechas
    /// </summary>
    public static class DateExtensions
    {
        /// <summary>
        /// Lista con meses en varios idiomas
        /// </summary>
        public static List<string[]> Meses = new List<string[]> {
            new string[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" },
            new string[] { "Urtarrila", "Otsaila", "Martxoa", "Apirila", "Maiatza", "Ekaina", "Uztaila", "Abuztua", "Iraila", "Urria", "Azaroa", "Abendua" },
            new string[] { "Xaneiro", "Febreiro", "Marzo", "Abril", "Maio", "Xuño", "Xullo", "Agosto", "Setembro", "Outubro", "Novembro", "Decembro" },
            new string[] { "Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre","Desembre" },
            new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }
        };

        /// <summary>
        /// Lista con días de la semana en varios idiomas
        /// </summary>
        public static List<string[]> DaysOfWeek = new List<string[]> {
            new string[] { "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" },
            new string[] { "Igandea", "Astelehena", "Asteartea", "Asteazkena", "Osteguna", "Ostirala", "Larunbata" },
            new string[] { "Domingo","Luns","Martes","Mércores", "Mércores", "Xoves", "Venres", "Sábado" },
            new string[] { "Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte" },
            new string[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" }
        };

        /// <summary>
        /// Devuelve literal con día de la semana
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idioma">Por si se quiere obtener el día en un idioma diferente</param>
        /// <returns></returns>
        public static string DiaDeLaSemana(this DateTime fecha, int idioma = 1)
            => DaysOfWeek[idioma - 1][(int)fecha.DayOfWeek];
        
        /// <summary>
        /// Devuelve literal con mes del año
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="idioma">Por si se quiere obtener el mes en un idioma diferente</param>
        /// <returns></returns>
        public static string MesDelAnio(this DateTime fecha, int idioma = 1)
            => Meses[idioma - 1][fecha.Month - 1];
        
        /// <summary>
        /// Calcula la edad en la actualidad respecto a la fecha de nacimiento
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static int CalcularEdad(this DateTime dateTime)
        {
            var age = DateTime.Now.Year - dateTime.Year;
            if (DateTime.Now < dateTime.AddYears(age))
                age--;
            return age;
        }

        /// <summary>
        /// Obtiene la fecha en formato: "Miércoles, 27 de Septiembre de 2017"
        /// </summary>
        /// <param name="date"></param>
        /// <param name="idioma">Por si se quiere obtener la fecha en un idioma diferente</param>
        /// <returns></returns>
        public static string FechaLarga(this DateTime date, int idioma = 1)
            => date.DiaDeLaSemana(idioma) + ", " + date.FechaLargaSinDiaSemana(idioma);

        /// <summary>
        /// Obtiene la fecha en formato: "27 de Septiembre de 2017"
        /// </summary>
        /// <param name="date"></param>
        /// <param name="idioma">Por si se quiere obtener la fecha en un idioma diferente</param>
        /// <returns></returns>
        public static string FechaLargaSinDiaSemana(this DateTime date, int idioma = 1)
            => date.Day + " de " + date.MesDelAnio(idioma) + " de " + date.Year;

        /// <summary>
        /// Obtiene la fecha en formato "27/09/2017"
        /// </summary>
        /// <param name="date"></param>
        /// <param name="separador"></param>
        /// <returns></returns>
        public static string FechaCorta(this DateTime date, char separador = '/')
        {
            string day = date.Day.ToString().Length == 1 ? "0" + date.Day : date.Day.ToString();
            string month = date.Month.ToString().Length == 1 ? "0" + date.Month : date.Month.ToString();
            return day + separador + month + separador + date.Year.ToString();
        }

        /// <summary>
        /// Devuelve hace cuando tiempo respecto al momento actual ocurrió una fecha
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string HaceCuantoTiempoExacto(this DateTime value)
        {
            var ts = new TimeSpan(DateTime.Now.Ticks - value.Ticks);
            double delta = ts.TotalSeconds;
            if (delta < 60)
                return ts.Seconds == 1 ? "hace un segundo" : "hace " + ts.Seconds  + " segundos";
            if (delta < 120)
                return "hace un minuto";
            if (delta < 2700) // 45 * 60
                return "hace " + ts.Minutes + " minutos";
            if (delta < 5400) // 90 * 60
                return "hace una hora";
            if (delta < 86400) // 24 * 60 * 60
                return "hace " + ts.Hours + " horas";
            if (delta < 172800) // 48 * 60 * 60
                return "ayer";
            if (delta < 2592000) // 30 * 24 * 60 * 60
                return "hace " + ts.Days + " días";
            if (delta < 31104000) // 12 * 30 * 24 * 60 * 60
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "hace un mes" : "hace " + months + " meses";
            }
            var years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
            return years <= 1 ? "hace un año" : "hace " + years + " años";
        }

        /// <summary>
        /// Determina si una fecha está entre otras dos fechas
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="rangeBeg">Fecha de comienzo</param>
        /// <param name="rangeEnd">Fecha final</param>
        /// <returns></returns>
        public static bool Between(this DateTime dt, DateTime rangeBeg, DateTime rangeEnd)
            => dt.Ticks >= rangeBeg.Ticks && dt.Ticks <= rangeEnd.Ticks;
        
        /// <summary>
        /// Determina si una fecha es día habil (de lunes a viernes generalmente)
        /// </summary>
        /// <param name="date"></param>
        /// <param name="diasFestivos">Lista de fechas festivas para tenerlas en cuenta</param>
        /// <returns></returns>
        public static bool EsDiaHabil(this DateTime date, List<DateTime> diasFestivos = null)
        {
            if (diasFestivos is null)
                diasFestivos = new List<DateTime>();
            return date.DayOfWeek != DayOfWeek.Saturday && date.DayOfWeek != DayOfWeek.Sunday &&  !diasFestivos.Contains(date);
        }

        /// <summary>
        /// Determina si una fecha es festiva (sábado o domingo normalmente)
        /// </summary>
        /// <param name="date"></param>
        /// <param name="diasFestivos">Lista de fechas festivas para tenerlas en cuenta</param>
        /// <returns></returns>
        public static bool EsFestivo(this DateTime date, List<DateTime> diasFestivos = null)
        {
            if (diasFestivos is null)
                diasFestivos = new List<DateTime>();
            return date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday || diasFestivos.Contains(date);
        }

        /// <summary>
        /// Devuelve el siguiente día habil
        /// </summary>
        /// <param name="date"></param>
        /// <param name="diasFestivos">Lista de fechas festivas para tenerlas en cuenta</param>
        /// <returns></returns>
        public static DateTime ProximoDiaHabil(this DateTime date,  List<DateTime> diasFestivos = null)
        {
            DateTime nextDay = date;
            while (!nextDay.EsDiaHabil(diasFestivos))
            {
                nextDay = nextDay.AddDays(1);
            }
            return nextDay;
        }

        /// <summary>
        /// Devuelve fecha habil después de sumar n número de días
        /// </summary>
        /// <param name="date"></param>
        /// <param name="numeroDias">Numero de dias que hay que sumar a la fecha</param>
        /// <param name="diasFestivos">Lista de fechas festivas para tenerlas en cuenta</param>
        /// <returns></returns>
        public static DateTime SumarDiasHabiles(this DateTime date, int numeroDias, List<DateTime> diasFestivos = null)
        {
            DateTime fecha = date;
            while (numeroDias > 0)
            {
                fecha.AddDays(1);
                if (fecha.EsDiaHabil(diasFestivos))
                    numeroDias--;
            }
            return fecha;
        }

        /// <summary>
        /// Devuelve el próximo día de la semana respecto a una fecha
        /// </summary>
        /// <param name="current"></param>
        /// <param name="dayOfWeek"></param>
        /// <returns></returns>
        public static DateTime Proximo(this DateTime current, DayOfWeek dayOfWeek)
        {
            int offsetDays = dayOfWeek - current.DayOfWeek;
            if (offsetDays <= 0)
            {
                offsetDays += 7;
            }
            DateTime result = current.AddDays(offsetDays);
            return result;
        }

        /// <summary>
        /// Lapso de tiempo desde la fecha indicada hasta la actualidad
        /// En negativo son fecha futuras
        /// </summary>
        /// <param name="fecha">Fecha</param>
        /// <returns>TimeSpan</returns>
        public static TimeSpan LapsoDeTiempo(this DateTime fecha) => DateTime.Now - fecha;
    }
}
