﻿using System.Linq;
using System.Text;

namespace System
{
    /// <summary>
    /// Métodos de utilidad para caracteres
    /// </summary>
    public static class CharExtensions
    {
        /// <summary>
        /// Repite x veces el caracter pasado como parámetro
        /// </summary>
        /// <param name="caracter"></param>
        /// <param name="numVeces">numero de veces que se repite</param>
        /// <returns></returns>
        public static string EscribirNumVeces(this char caracter, int numVeces)
        {
            StringBuilder res = new StringBuilder();
            foreach (var x in Enumerable.Range(0, numVeces))
                res.Append(caracter);
            return res.ToString();
        }
    }
}
