﻿using System;
using System.Collections.Generic;

namespace System
{
    /// <summary>
    /// Métodos útiles de excepciones
    /// </summary>
    public static class ExceptionExtensions
    {
        /// <summary>
        /// Devuelve todos los mensajes de las excepciones internas de una excepción
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static IEnumerable<string> Mensajes(this Exception ex)
        {
            if (ex == null)
                throw new ArgumentNullException("Excepción es nula");
            var innerException = ex;
            do
            {
                yield return innerException.Message;
                innerException = innerException.InnerException;
            }
            while (innerException != null);
        }

        /// <summary>
        /// Lanza una excepcion ArgumentNullException si el campo está vacío
        /// </summary>
        /// <param name="o">el campo o parámetro</param>
        /// <param name="nombreCampo">Nombre del campo o parámetro (nameof(nombreCampo))</param>
        public static void ExcepcionArgumentoVacio(object o, string nombreCampo)
        {
            if (o is string && string.IsNullOrWhiteSpace((string)o))
                throw new ArgumentNullException(nombreCampo, $"El campo {nombreCampo} no puede estar vacío");
            if (o == null)
                throw new ArgumentNullException(nombreCampo, $"El campo {nombreCampo} no puede estar vacío");
        }

        /// <summary>
        /// Lanza una excepcion ArgumentException si el objeto buscado es nulo
        /// </summary>
        /// <param name="o">el campo o parámetro</param>
        /// <param name="id">id utilizado en la búsqueda</param>
        public static void ExcepcionNoEncontrado(object o, object id)
        {
            if (o == null)
                throw new ArgumentException($"No se encontró valor con id = {id}");
        }

        //public static void UltimoMensaje(this Exception ex, string titulo = "Error", MessageBoxIcon icon = MessageBoxIcon.Error)
        //=> MessageBox.Show(ex.Mensajes().LastOrDefault() ?? "", titulo, MessageBoxButtons.OK, icon);


        //public static void TodosLosMensajes(this Exception ex, string titulo = "Error", MessageBoxIcon icon = MessageBoxIcon.Error)
        //=> MessageBox.Show(string.Join("\n", ex.Mensajes()), titulo, MessageBoxButtons.OK, icon);
    }
}
