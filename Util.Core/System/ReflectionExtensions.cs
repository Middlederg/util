﻿using System.Reflection;

namespace System.Reflection
{
    /// <summary>
    /// Métodos de ayuda en los que se utiliza la reflexión
    /// </summary>
    public static class ReflectionExtensions
    {
        /// <summary>
        /// Determina si dos propiedades de un objeto tienen el mismo contenido
        /// </summary>
        /// <typeparam name="T">Clase</typeparam>
        /// <param name="p">Primera propiedad</param>
        /// <param name="obj1">Primera instancia de la clase</param>
        /// <param name="obj2">Segunda instancia de la propiedad</param>
        /// <returns></returns>
        public static bool MismoContenido<T>(this PropertyInfo p, T obj1, T obj2)
        {
            //Ambos objetos son null
            if (p.GetValue(obj1, null) == null && p.GetValue(obj2, null) == null) return true;

            //Un objeto es null y el otro no
            if (p.GetValue(obj1, null) == null && p.GetValue(obj2, null) != null) return false;
            if (p.GetValue(obj1, null) != null && p.GetValue(obj2, null) == null) return false;

            //Comparar
            return (p.GetValue(obj1, null).Equals(p.GetValue(obj2, null)));
        }

        /// <summary>
        /// Devuelve el contenido de una propiedad en formato string.
        /// Si es null, devuelve vacío
        /// </summary>
        /// <typeparam name="T">Clase</typeparam>
        /// <param name="p">Propiedad</param>
        /// <param name="obj">instancia de la clase</param>
        /// <returns></returns>
        public static string Contenido<T>(this PropertyInfo p, T obj)
        {
            if (p.GetValue(obj, null) == null) return "";
            if (p.PropertyType.Equals(typeof(DateTime)) || p.PropertyType.Equals(typeof(DateTime?))) return ((DateTime)p.GetValue(obj, null)).ToShortDateString();
            return p.GetValue(obj, null).ToString();
        }
    }
}
