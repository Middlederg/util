﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Util.Core.Data
{
    /// <summary>
    /// Conexión para archivo mdf
    /// </summary>
    public class Conexion : IDisposable
    {
        /// <summary>
        /// SqlConnection
        /// </summary>
        protected SqlConnection connection;

        /// <summary>
        /// Conexión a fichero .MDF
        /// </summary>
        /// <param name="mdfPath">ruta completa de ubicación del fichero .MDF</param>
        public Conexion(string mdfPath)
        {
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder()
                {
                    ConnectionString = @"Data Source=.\SQLEXPRESS;" +
                                $@"AttachDbFilename=|DataDirectory|{mdfPath};" +
                           // @"AttachDbFilename=C:\Users\Jorge\Documents\Visual Studio 2013\Projects\BoardGameManager\" + mdf + ";" +
                          "Integrated Security=True;" +
                          "Connect Timeout=30;" +
                         "User Instance=True"
                };
                connection = new SqlConnection(builder.ConnectionString);
            }
            catch (SqlException e) { throw new Exception("Error inicializando la conexión\n" + e.Message); }
        }

        private void AbrirConexion()
        {
            try
            {
                //Me aseguro que la conexión está cerrada antes de abrirla
                if (connection.State == ConnectionState.Open)
                    connection.Close();

                connection.Open();
            }
            catch (SqlException e)
            {
                throw new Exception("Error abriendo conexión\n" + e.Message);
            }
        }


        private void CerrarConexion()
        {
            try
            {
                //Me aseguro que la conexión está abierta antes de cerrarla
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
            catch (SqlException e)
            {
                throw new Exception("Error cerrando conexión\n" + e.Message);
            }
        }

        #region SELECT

        /// <summary>
        /// Select genérica
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selectSQL"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> Select<T>(string selectSQL)
        {
            List<T> lista = new List<T>();

            AbrirConexion();
            using (SqlCommand command = new SqlCommand(selectSQL, connection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        T objeto = (T)Activator.CreateInstance(typeof(T)); //ojo, que debe de haber constructor sin parametros definido para ese objeto
                        int indxColumna = 0;
                        Type t = typeof(T);
                        foreach (PropertyInfo propiedad in t.GetProperties())
                        {
                            if (reader[propiedad.Name] is System.DBNull)
                            {
                                propiedad.SetValue(objeto, null, null);
                            }
                            else
                            {
                                propiedad.SetValue(objeto, reader[propiedad.Name], null);
                            }
                            indxColumna++;
                        }
                        lista.Add(objeto);
                    }
                }
            }
            CerrarConexion();
            return lista;
        }

        /// <summary>
        /// Select de un solo campo (string)
        /// </summary>
        /// <param name="selectSQL"></param>
        /// <returns></returns>
        public virtual string SelectUnValor(string selectSQL)
        {
            string result = null;

            AbrirConexion();
            using (SqlCommand command = new SqlCommand(selectSQL, connection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                        result = reader[0].ToString();
                }
            }
            CerrarConexion();
            return result;
        }

        /// <summary>
        /// Select de un solo campo (int)
        /// </summary>
        /// <param name="selectSQL"></param>
        /// <returns></returns>
        public int? SelectInt(string selectSQL)
        {
            int? result = null;

            AbrirConexion();
            using (SqlCommand command = new SqlCommand(selectSQL, connection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        result = reader[0] is System.DBNull ? null : (int?)reader[0];
                    }
                }
            }
            CerrarConexion();
            return result;
        }

        /// <summary>
        /// Select de un solo campo (bigint o long)
        /// </summary>
        /// <param name="selectSQL"></param>
        /// <returns></returns>
        public long? SelectLong(string selectSQL)
        {
            long? result = null;

            AbrirConexion();
            using (SqlCommand command = new SqlCommand(selectSQL, connection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        result = reader[0] is System.DBNull ? null : (long?)reader[0];
                    }
                }
            }
            CerrarConexion();
            return result;
        }

        /// <summary>
        /// Select de un solo campo (Decimal)
        /// </summary>
        /// <param name="selectSQL"></param>
        /// <returns></returns>
        public Decimal? SelectDecimal(string selectSQL)
        {
            decimal? result = null;

            AbrirConexion();
            using (SqlCommand command = new SqlCommand(selectSQL, connection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        result = reader[0] is System.DBNull ? null : (decimal?)reader[0];
                    }
                }
            }
            CerrarConexion();
            return result;
        }

        /// <summary>
        /// Select de un solo campo (small)
        /// </summary>
        /// <param name="selectSQL"></param>
        /// <returns></returns>
        public Int16? SelectSmallInt(string selectSQL)
        {
            Int16? result = null;

            AbrirConexion();
            using (SqlCommand command = new SqlCommand(selectSQL, connection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        result = reader[0] is System.DBNull ? null : (Int16?)reader[0];
                    }
                }
            }
            CerrarConexion();
            return result;
        }

        /// <summary>
        /// Select de un solo campo (numérico)
        /// </summary>
        /// <param name="selectSQL"></param>
        /// <returns></returns>
        public int? SelectScalar(string selectSQL)
        {
            int? result = null;

            AbrirConexion();
            using (SqlCommand command = new SqlCommand(selectSQL, connection))
            {
                result = Convert.ToInt32(command.ExecuteScalar());
            }
            CerrarConexion();
            return result;
        }

        /// <summary>
        /// Devuelve el Id máximo de la tabla
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="campoId">Campo que es la clave primaria</param>
        /// <returns></returns>
        public long MaxId<T>(string campoId)
        {
            return (long)SelectLong("select max(" + campoId + ") from " + typeof(T).Name);
        }

        #endregion

        #region INSERT

        /// <summary>
        /// Inserta un objeto en la Base de datos
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">Objeto a insertar</param>
        /// <returns></returns>
        public virtual bool Insert<T>(T obj)
        {
            int rowsAffected;
            AbrirConexion();

            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT " + typeof(T).Name + "(");
            foreach (PropertyInfo propiedad in typeof(T).GetProperties()) //nombres tabla
            {
                if (!(propiedad.GetValue(obj, null) is null))
                    sb.Append(propiedad.Name + ",");
            }
            sb.Remove(sb.Length - 1, 1); //quito la última coma
            sb.Append(") VALUES (");
            int numCol = 1;
            foreach (PropertyInfo propiedad in typeof(T).GetProperties()) //valores a insertar
            {
                if (!(propiedad.GetValue(obj, null) is null))
                {
                    sb.Append("@V" + numCol + ",");
                    numCol++;
                }
            }
            sb.Remove(sb.Length - 1, 1); //quito la última coma
            sb.Append(");");

            using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
            {
                numCol = 1;
                foreach (PropertyInfo propiedad in typeof(T).GetProperties())
                {
                    if (!(propiedad.GetValue(obj, null) is null))
                    {
                        command.Parameters.AddWithValue("@V" + numCol, propiedad.GetValue(obj, null));
                        numCol++;
                    }
                }
                rowsAffected = command.ExecuteNonQuery();
            }

            CerrarConexion();

            return rowsAffected > 0;
        }

        /// <summary>
        /// Insertar un objeto sin tener en cuenta la clave primaria
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="campoId"></param>
        /// <returns></returns>
        public virtual bool InsertAutonumerico<T>(T obj, string campoId)
        {
            int rowsAffected;
            AbrirConexion();

            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT " + typeof(T).Name + "(");
            for (int i = 0; i < typeof(T).GetProperties().Length; i++)
            {
                PropertyInfo prop = typeof(T).GetProperties()[i];
                if (!prop.Name.ToUpper().Equals(campoId.ToUpper()) && (!(prop.GetValue(obj, null) is null)))
                    sb.Append(prop.Name + ",");
            }
            sb.Remove(sb.Length - 1, 1); //quito la última coma
            sb.Append(") VALUES (");
            int numCol = 1;
            for (int i = 0; i < typeof(T).GetProperties().Length; i++) //valores a insertar
            {
                PropertyInfo prop = typeof(T).GetProperties()[i];
                if (!prop.Name.ToUpper().Equals(campoId.ToUpper()) && !(prop.GetValue(obj, null) is null))
                {
                    sb.Append("@V" + numCol + ",");
                    numCol++;
                }
            }
            sb.Remove(sb.Length - 1, 1); //quito la última coma
            sb.Append(");");

            using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
            {
                numCol = 1;
                for (int i = 0; i < typeof(T).GetProperties().Length; i++)
                {
                    PropertyInfo prop = typeof(T).GetProperties()[i];
                    if (!prop.Name.ToUpper().Equals(campoId.ToUpper()) && !(prop.GetValue(obj, null) is null))
                    {
                        command.Parameters.AddWithValue("@V" + numCol, prop.GetValue(obj, null));
                        numCol++;
                    }
                }
                rowsAffected = command.ExecuteNonQuery();
            }

            CerrarConexion();

            return rowsAffected > 0;
        }

        /// <summary>
        /// Inserta una lista de objetos
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lista"></param>
        /// <returns></returns>
        public int InsertMultiple<T>(List<T> lista)
        {
            int totalFilasInsertadas = 0;
            AbrirConexion();

            foreach (T obj in lista)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("INSERT " + typeof(T).Name + "(");
                foreach (PropertyInfo propiedad in typeof(T).GetProperties()) //nombres tabla
                {
                    if (!(propiedad.GetValue(obj, null) is null))
                        sb.Append(propiedad.Name + ",");
                }
                sb.Remove(sb.Length - 1, 1); //quito la última coma
                sb.Append(") VALUES (");
                int numCol = 1;
                foreach (PropertyInfo propiedad in typeof(T).GetProperties()) //valores a insertar
                {
                    if (!(propiedad.GetValue(obj, null) is null))
                    {
                        sb.Append("@V" + numCol + ",");
                        numCol++;
                    }
                }
                sb.Remove(sb.Length - 1, 1); //quito la última coma
                sb.Append(");");

                using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                {
                    numCol = 1;
                    foreach (PropertyInfo propiedad in typeof(T).GetProperties())
                    {
                        if (!(propiedad.GetValue(obj, null) is null))
                        {
                            command.Parameters.AddWithValue("@V" + numCol, propiedad.GetValue(obj, null));
                            numCol++;
                        }
                    }
                    int rowsAffected = command.ExecuteNonQuery();
                    totalFilasInsertadas += rowsAffected;
                }
            }
            CerrarConexion();

            return totalFilasInsertadas;
        }

        #endregion

        #region UPDATE

        /// <summary>
        /// Actualiza una lista de objetos
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="campoId"></param>
        /// <returns></returns>
        public virtual bool Update<T>(T obj, string campoId)
        {
            int rowsAffected;
            AbrirConexion();

            string id;
            try
            {
                id = typeof(T).GetProperties().First(x => x.Name.ToUpper().Equals(campoId.ToUpper())).GetValue(obj, null).ToString();
            }
            catch (Exception ex)
            {
                throw new ArgumentException("No se pudo obtener la clave primaria de la tabla " + typeof(T).Name + "\n\n" + ex.Message);
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE " + typeof(T).Name + " SET ");

            for (int i = 0; i < typeof(T).GetProperties().Length; i++) //valores actualizar, todos menos el id
            {
                string propiedad = typeof(T).GetProperties()[i].Name;
                if (!propiedad.ToUpper().Equals(campoId.ToUpper()))
                {
                    sb.Append(typeof(T).GetProperties()[i].Name + " = ");
                    sb.Append("@V" + i + ",");
                }
            }
            sb.Remove(sb.Length - 1, 1); //quito la última coma
            sb.Append(" WHERE " + campoId + " = " + id + ";");

            using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
            {
                for (int i = 0; i < typeof(T).GetProperties().Length; i++) //valores actualizar, todos menos el id
                {
                    PropertyInfo propiedad = typeof(T).GetProperties()[i];
                    if (!propiedad.Name.ToUpper().Equals(campoId.ToUpper()))
                    {
                        if (!(propiedad.GetValue(obj, null) is null))
                            command.Parameters.AddWithValue("@V" + i, propiedad.GetValue(obj, null));
                        else
                        {
                            if (propiedad.PropertyType == typeof(byte[]))
                                command.Parameters.Add("@V" + i, SqlDbType.Image).Value = DBNull.Value;
                            else
                                command.Parameters.AddWithValue("@V" + i, DBNull.Value);
                        }
                    }
                }
                rowsAffected = command.ExecuteNonQuery();
            }

            CerrarConexion();
            return rowsAffected > 0;
        }


        /// <summary>
        /// Actualiza una lista de objetos
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lista"></param>
        /// <param name="campoId"></param>
        /// <returns></returns>
        public int UpdateMultiple<T>(List<T> lista, string campoId)
        {
            int numFilasActualizadas = 0;
            string ids = ";";
            AbrirConexion();

            foreach (T obj in lista)
            {
                string id;
                try
                {
                    id = typeof(T).GetProperties().First(x => x.Name.Equals(campoId)).GetValue(obj, null).ToString();
                }
                catch (Exception ex)
                {
                    throw new ArgumentException("No se pudo obtener la clave primaria de la tabla " + typeof(T).Name + "\n\n" + ex.Message);
                }

                StringBuilder sb = new StringBuilder();
                sb.Append("UPDATE " + typeof(T).Name + " SET ");

                for (int i = 0; i < typeof(T).GetProperties().Length; i++) //valores actualizar, todos menos el id
                {
                    string propiedad = typeof(T).GetProperties()[i].Name;
                    if (!propiedad.ToUpper().Equals(campoId.ToUpper()))
                    {
                        sb.Append(propiedad + " = ");
                        sb.Append("@V" + i + ",");
                    }
                }
                sb.Remove(sb.Length - 1, 1); //quito la última coma
                sb.Append(" WHERE " + campoId + " = " + id + ";");

                using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                {
                    for (int i = 0; i < typeof(T).GetProperties().Length; i++) //valores actualizar, todos menos el id
                    {
                        PropertyInfo propiedad = typeof(T).GetProperties()[i];
                        if (!propiedad.Name.ToUpper().Equals(campoId.ToUpper()))
                        {
                            if (!(propiedad.GetValue(obj, null) is null))
                                command.Parameters.AddWithValue("@V" + i, propiedad.GetValue(obj, null));
                            else
                            {
                                if (propiedad.PropertyType == typeof(byte[]))
                                    command.Parameters.Add("@V" + i, SqlDbType.Image).Value = DBNull.Value;
                                else
                                    command.Parameters.AddWithValue("@V" + i, DBNull.Value);
                            }
                        }
                    }
                    int rowsAffected = command.ExecuteNonQuery();
                    numFilasActualizadas += rowsAffected;
                    if (rowsAffected > 0)
                        ids += id + ", ";
                }
            }

            CerrarConexion();
            return numFilasActualizadas;
        }

        #endregion

        #region BORRAR

        /// <summary>
        /// Da de baja un registro, suponiendo que hay un campo DateTime que indica la fecha en la que se dió de baja
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="campoId"></param>
        /// <param name="fechaBaja"></param>
        /// <returns></returns>
        public bool DarDeBaja<T>(T obj, string campoId, string fechaBaja = "FECHABAJA")
        {
            int rowsAffected;
            AbrirConexion();

            string id;
            try
            {
                id = typeof(T).GetProperties().First(x => x.Name.Equals(campoId)).GetValue(obj, null).ToString();
            }
            catch (Exception ex)
            {
                throw new ArgumentException("No se pudo obtener la clave primaria de la tabla " + typeof(T).Name + "\n\n" + ex.Message);
            }

            string sqlSentencia = "UPDATE " + typeof(T).Name + " SET " + fechaBaja + " = '" + DateTime.Now.ToString() + "' WHERE " + campoId + " = @idTabla";

            using (SqlCommand command = new SqlCommand(sqlSentencia, connection))
            {
                command.Parameters.AddWithValue("@idTabla", id);
                rowsAffected = command.ExecuteNonQuery();
            }

            CerrarConexion();
            return rowsAffected > 0;
        }

        /// <summary>
        /// Elimina el objeto indicado 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="campoId"></param>
        /// <returns></returns>
        public bool Eliminar<T>(T obj, string campoId)
        {
            int rowsAffected;
            AbrirConexion();

            string id;
            try
            {
                id = typeof(T).GetProperties().First(x => x.Name.Equals(campoId)).GetValue(obj, null).ToString();
            }
            catch (Exception ex)
            {
                throw new ArgumentException("No se pudo obtener la clave primaria de la tabla " + typeof(T).Name + "\n\n" + ex.Message);
            }

            string sqlSentencia = "DELETE " + typeof(T).Name + " WHERE " + campoId + " = @idTabla";

            using (SqlCommand command = new SqlCommand(sqlSentencia, connection))
            {
                command.Parameters.AddWithValue("@idTabla", id);
                rowsAffected = command.ExecuteNonQuery();
            }

            CerrarConexion();
            return rowsAffected > 0;
        }

        /// <summary>
        /// Elimina el registro indicado cuya clave primaría coincide con el id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <param name="campoId"></param>
        /// <returns></returns>
        public bool Eliminar<T>(long id, string campoId)
        {
            int rowsAffected;
            AbrirConexion();

            string sqlSentencia = "DELETE " + typeof(T).Name + " WHERE " + campoId + " = @idTabla";

            using (SqlCommand command = new SqlCommand(sqlSentencia, connection))
            {
                command.Parameters.AddWithValue("@idTabla", id);
                rowsAffected = command.ExecuteNonQuery();
            }

            CerrarConexion();
            return rowsAffected > 0;
        }

        /// <summary>
        /// Elimina todos los registros de una tabla
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public int EliminarTabla<T>()
        {
            int rowsAffected;
            AbrirConexion();

            string sqlSentencia = "DELETE " + typeof(T).Name;

            using (SqlCommand command = new SqlCommand(sqlSentencia, connection))
            {
                rowsAffected = command.ExecuteNonQuery();
            }

            CerrarConexion();
            return rowsAffected;
        }

        #endregion

        /// <summary>
        /// Realiza una sentencia SQL
        /// </summary>
        /// <param name="sqlSentencia"></param>
        /// <returns></returns>
        public bool SentenciaLibre(string sqlSentencia)
        {
            int rowsAffected;
            AbrirConexion();

            using (SqlCommand command = new SqlCommand(sqlSentencia, connection))
            {
                rowsAffected = command.ExecuteNonQuery();
            }

            CerrarConexion();
            return rowsAffected > 0;
        }

        /// <summary>
        /// Cierra la conexión y libera los recursos
        /// </summary>
        public void Dispose()
        {
            connection.Close();
            connection.Dispose();
        }
    }
}

