﻿using System.Collections.Generic;
using System.Linq;

namespace System.Collections.Generic
{
    /// <summary>
    /// Métodos de utilidad para colecciones
    /// </summary>
    public static class Colecciones
    {
        /// <summary>
        /// Convierte a string una lista en formato 1, 2, 3...)
        /// </summary>
        /// <param name="lista"></param>
        /// <returns></returns>
        public static string ListaConComas<T>(this IEnumerable<T> lista)
        {
            if (lista == null || lista.Count() == 0) return string.Empty;
            return string.Join(", ", lista);
        }

        /// <summary>
        /// Convierte a string una lista separando cada elemento por un punto y aparte
        /// </summary>
        /// <param name="lista"></param>
        /// <returns></returns>
        public static string ListaConFilas<T>(this IEnumerable<T> lista)
        {
            if (lista == null || lista.Count() == 0) return string.Empty;
            return string.Join("\n", lista);
        }
    }
}
