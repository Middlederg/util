﻿namespace System
{
    /// <summary>
    /// Clase singleton para la obtención de métodos aleatorios
    /// </summary>
    public class R
    {
        private static readonly Lazy<R> lazy = new Lazy<R>(() => new R());

        /// <summary>
        /// Recupera la instancia singleton de la clase que genera aleatoriedad
        /// </summary>
        public static R Instance { get { return lazy.Value; } }

        private R() { Azar = new Random(DateTime.Now.Millisecond); }

        /// <summary>
        /// Obtiene variable de tipo Random instanciada
        /// </summary>
        public Random Azar { get; }

        /// <summary>
        /// Numero aleatorio entre dos números, incluidos ambos
        /// </summary>
        /// <param name="inf">Número inferior</param>
        /// <param name="sup">Número superior</param>
        /// <returns></returns>
        public int NumAleatorio(int inf, int sup) => Azar.Next(inf, sup + 1);

        /// <summary>
        /// Sortea el jugador inicial de la partida
        /// </summary>
        /// <param name="numJugadores"></param>
        /// <returns></returns>
        public int PrepararJugadorInicial(int numJugadores) => NumAleatorio(0, numJugadores - 1);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="desde">año desde (incluído)</param>
        /// <param name="hasta">año hasta (incluído)</param>
        /// <returns></returns>
        public DateTime FechaAleatoria(int desde, int hasta)
        {
            int year = NumAleatorio(desde, hasta);
            int mes = NumAleatorio(1, 12);
            int dia;
            if (mes == 4 || mes == 6 || mes == 9 || mes == 11) dia = NumAleatorio(1, 30);
            else if (mes == 2) dia = NumAleatorio(1, 28);
            else dia = NumAleatorio(1, 31);
            return new DateTime(year, mes, dia);
        }
    }
}
