﻿using System.Collections.Generic;
using System.Linq;

namespace System
{
    /// <summary>
    /// Métodos de ayuda para la clase Random
    /// </summary>
    public static class RandomHelper
    {
        /// <summary>
        /// Obtiene un elemento aleatorio de una lista
        /// </summary>
        /// <typeparam name="T">Tipo de objeto</typeparam>
        /// <param name="lista">Lista de objetos</param>
        /// <returns></returns>
        public static T ElementoAleatorio<T>(this IEnumerable<T> lista)
        {
            if (lista == null || !lista.Any()) return default;
            return lista.ElementAt(R.Instance.NumAleatorio(0, lista.Count() - 1));
        }

        /// <summary>
        /// Obtiene lista de n de elementos aleatorios
        /// </summary>
        /// <param name="lista"></param>
        /// <param name="numero"></param>
        /// <returns></returns>
        public static IEnumerable<T> ElementosAleatorios<T>(this IEnumerable<T> lista, int numero)
        {
            if (lista == null || !lista.Any()) yield break;
            var copia = lista.ToList();
            foreach (int i in Enumerable.Range(0, numero))
            {
                T e = ElementoAleatorio(copia);
                copia.Remove(e);
                yield return e;
            }
        }

        /// <summary>
        /// Desordena una lista de elementos
        /// </summary>
        /// <typeparam name="T">Tipo de objeto</typeparam>
        /// <param name="lista">Lista de objetos</param>
        /// <returns></returns>
        public static List<T> Desordenar<T>(this List<T> lista)
        {
            if (lista == null) return null;
            if (lista.Count == 0) return new List<T>();
            List<T> listaDesordenada = new List<T>();

            while (lista.Count > 0)
            {
                T e = ElementoAleatorio(lista);
                listaDesordenada.Add(e);
                lista.Remove(e);
            }
            return listaDesordenada;
        }

        /// <summary>
        /// Puede devolver verdadero el porcentaje indicado
        /// </summary>
        /// <param name="porcentaje">Porcentaje entre 1 y 100</param>
        /// <returns></returns>
        public static bool PorCiento(this int porcentaje)
        {
            if (porcentaje < 1 || porcentaje > 100)
                throw new ArgumentOutOfRangeException("Debe ser un número entre 1 y 100");
            int resultado = R.Instance.NumAleatorio(1, 100);
            return resultado <= porcentaje;
        }      
    }
}
