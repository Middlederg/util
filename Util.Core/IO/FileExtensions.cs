﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace System.IO
{
    /// <summary>
    /// Métodos de utilidad para ficheros
    /// </summary>
    public static class Ficheros
    {
        ///// <summary>
        ///// Obtiene un fichero de texto guardado como recurso en el proyecto
        ///// </summary>
        ///// <param name="nombre"></param>
        ///// <returns></returns>
        //public static string GetTextResourceFile(string nombre) => (string)Util.Core.Properties.Resources.ResourceManager.GetObject(nombre);

        ///// <summary>
        ///// Obtiene stream de un fichero guardado como recurso en el proyecto
        ///// </summary>
        ///// <param name="nombre">Nombre del recurso en el ensamblado LabnetComun</param>
        ///// <returns></returns>
        //public static Stream GetStreamResourceFile(string nombre) => (Stream)Util.Core.Properties.Resources.ResourceManager.GetObject(nombre);


        /// <summary>
        /// Devuelve lista con todas las lineas del fichero 
        /// Por defecto toma la primera linea como cabecera y se la salta
        /// </summary>
        /// <param name="ruta"></param>
        /// <param name="cabecera">Poner a false cuando el fichero venga sin cabeceras</param>
        /// <returns></returns>
        public static List<string> LeerFichero(string ruta, bool cabecera = true)
        {
            List<string> origen = new List<string>();
            StreamReader sr = new StreamReader(ruta, System.Text.Encoding.GetEncoding(1252));
            if (cabecera)
                sr.ReadLine();
            while (!sr.EndOfStream)
                origen.Add(sr.ReadLine());
            sr.Close();
            return origen;
        }

        /// <summary>
        /// Escribe fichero con todos los elementos de la lista
        /// </summary>
        /// <param name="filas"></param>
        /// <param name="ruta"></param>
        public static void EscribirEnFichero(this List<string> filas, string ruta)
        {
            File.Delete(ruta);
            StreamWriter escritor = new StreamWriter(ruta, false, Encoding.GetEncoding(1252)) { AutoFlush = true };
            foreach (string s in filas)
                escritor.WriteLine(s);
            escritor.Close();
        }

        /// <summary>
        /// Escribe fichero con todos los elementos de la lista sin sobreescribirlo
        /// </summary>
        /// <param name="filas"></param>
        /// <param name="ruta"></param>
        public static void AnexarEnFichero(this List<string> filas, string ruta)
        {
            StreamWriter escritor = File.AppendText(ruta);
            escritor.AutoFlush = true;
            foreach (string s in filas)
                escritor.WriteLine(s);
            escritor.Close();
        }

    }
}
