﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace System.Collections.Generic
{
    /// <summary>
    /// Métodos de utilidad para ficheros csv
    /// </summary>
    public static class CsvExtensions
    {
        /// <summary>
        /// Convierte una lista de objetos a csv, con una columna por cada propiedad
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lista">lista de objetos cualesquiera</param>
        /// <param name="conCabecera">Determina si la primera linea será cabecera</param>
        /// <returns>Devuelve una lista de filas con columnas separadas por comas</returns>
        public static List<string> ConvertToCsv<T>(this List<T> lista, bool conCabecera = false)
        {
            List<string> res = new List<string>();

            string fila = string.Empty;
            if (conCabecera)
                foreach (PropertyInfo propiedad in typeof(T).GetProperties())
                    fila += propiedad.Name + ";";
            res.Add(fila);

            foreach (T obj in lista)
            {
                fila = string.Empty;
                foreach (PropertyInfo propiedad in obj.GetType().GetProperties())
                {
                    if (propiedad.GetValue(obj, null) is null)
                        fila += "";
                    else
                        fila += propiedad.GetValue(obj, null).ToString().Replace(';', ',');
                    fila += ";";
                }
                res.Add(fila);
            }
            return res;
        }
    }
}
