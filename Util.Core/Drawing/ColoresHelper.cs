﻿namespace System.Drawing
{
    /// <summary>
    /// Métodos de utilidad para manipular colores
    /// </summary>
    public static class ColoresHelper
    {
        /// <summary>
        /// Aclara en el grado establecido un color
        /// </summary>
        /// <param name="color">Color a cambiar</param>
        /// <param name="grado">Grado a aclarar</param>
        /// <returns></returns>
        public static Color Aclarar(this Color color, int grado) => Cambiar(color, grado * (-1));

        /// <summary>
        /// Oscurece en el grado establecido un color
        /// </summary>
        /// <param name="color">Color a cambiar</param>
        /// <param name="grado">Grado a oscurecer</param>
        /// <returns></returns>
        public static Color Oscurecer(this Color color, int grado) => Cambiar(color, grado);

        private static Color Cambiar(this Color color, int cantidad)
            => Color.FromArgb(OperarColor(color.R, cantidad), OperarColor(color.G, cantidad), OperarColor(color.B, cantidad));
        
        private static int OperarColor(int valor, int cambio)
        {
            if (valor - cambio < 0) return 0;
            if (valor - cambio > 255) return 255;
            return valor - cambio;
        }
    }
}
