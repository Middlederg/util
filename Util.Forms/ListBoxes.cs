﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Util.Forms.Model;

namespace System.Windows.Forms
{
    public static class ListBoxes
    {
        public static void FillListBox(ListBox lbx, List<ComboBaseItem> lista, ComboBaseItem defaultValue = null)
        {
            lbx.BeginUpdate();
            lbx.DisplayMember = "Text";
            lbx.ValueMember = "Value";
            if (defaultValue != null)
                lista.Insert(0, defaultValue);
            var items = lista;
            lbx.DataSource = items;
            lbx.EndUpdate();
        }

        public static int GetValueFromListBox(ListBox lbx)
        {
            int selected = (int)lbx.SelectedValue;
            return selected;
        }

        public static void SetListBoxValue(this ComboBox cmb, int value) => cmb.SelectedValue = value;
    }
}
