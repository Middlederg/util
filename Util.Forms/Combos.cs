﻿using System.Collections.Generic;
using System.Linq;
using System;
using Util.Forms.Model;

namespace System.Windows.Forms
{
    public static class Combos
    {
        /// <summary>
        /// Llena un combo con los valores indicados
        /// </summary>
        /// <param name="cmb">Combo</param>
        /// <param name="lista">Lista de Tuplas Valor - Texto</param>
        /// <param name="defaultValue">Valor por defecto</param>
        public static void FillCombo(ComboBox cmb, List<ComboBaseItem> lista, ComboBaseItem defaultValue = null)
        {
            cmb.BeginUpdate();
            cmb.DisplayMember = "Text";
            cmb.ValueMember = "Value";
            if (defaultValue != null)
                lista.Insert(0, defaultValue);
            cmb.DataSource = lista;
            cmb.EndUpdate();
        }

        /// <summary>
        /// Obtiene el elemento seleccionado en un combo
        /// </summary>
        /// <param name="cmb">Combo</param>
        /// <returns></returns>
        public static int GetValueFromCombo(this ComboBox cmb)
        {
            int selected = (int)cmb.SelectedValue;
            return selected;
        }

        /// <summary>
        /// Establece valor en un combo
        /// </summary>
        /// <param name="cmb">Combo</param>
        /// <param name="value">Valor a establecer</param>
        public static void SetCombo(this ComboBox cmb, int value) => cmb.SelectedValue = value;

        /// <summary>
        /// Determina si el valor está en el combo
        /// </summary>
        /// <param name="cmb"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsValueInCombo(this ComboBox cmb, int value) 
            => cmb.Items.Cast<ComboBaseItem>().Any(x=> x.Value == value);

    }
}
