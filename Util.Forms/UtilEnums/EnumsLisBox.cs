﻿using System;
using System.Linq;
using Util.Forms.Model;

namespace System.Windows.Forms
{
    public static class EnumsLisBox
    {
        public static void LlenarListBox<T>(ListBox lbx) where T : Enum
        {
            var todosLosValores = EnumExtensions.GetEnumList<T>();
            var lista = todosLosValores.Select(x => new ComboBaseItem() { Value = (Convert.ToInt32(Enum.Parse(typeof(T), x.ToString()))) });
            ListBoxes.FillListBox(lbx, lista.ToList());
        }

        public static void LlenarListBoxDisplay<T>(ListBox lbx) where T : Enum
        {
            var todosLosValores = EnumExtensions.GetEnumListDictionary<T>();
            var lista = todosLosValores.Select(x => new ComboBaseItem() { Value = (Convert.ToInt32(Enum.Parse(typeof(T), x.value.ToString()) as Enum)), Text = x.display });
            ListBoxes.FillListBox(lbx, lista.ToList());
        }

        public static T GetEnumFromListBox<T>(this ListBox lbx) where T : Enum
        {
            T selected = (T)(object)((int)lbx.SelectedValue);
            return selected;
        }

        public static void SetEnumFromListBox<T>(this ListBox lbx, T value) where T : Enum
        {
            lbx.SelectedValue = (Convert.ToInt32(Enum.Parse(typeof(T), value.ToString()) as Enum));
        }
    }
}
