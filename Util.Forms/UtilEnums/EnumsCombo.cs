﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Util.Forms.Model;

namespace System.Windows.Forms
{
    public static class EnumsCombo
    {
        /// <summary>
        /// Llena un combo con los valores ToString() de una enumeración
        /// </summary>
        /// <typeparam name="T">Enum</typeparam>
        /// <param name="cmb">Combo a rellenar</param>
        /// <param name="defaultValue">Valor por defecto</param>
        public static void LlenarCombo<T>(ComboBox cmb, ComboBaseItem defaultValue = null) where T : Enum
        {
            var todosLosValores = EnumExtensions.GetEnumList<T>();
            var lista = todosLosValores.Select(x => new ComboBaseItem() { Value = (Convert.ToInt32(Enum.Parse(typeof(T), x.ToString()) as Enum)), Text = x.ToString() }).ToList();
            Combos.FillCombo(cmb, lista);
        }

        /// <summary>
        /// Llena un combo con los atributos Display de una enumeración
        /// </summary>
        /// <typeparam name="T">Enum</typeparam>
        /// <param name="cmb">Combo a rellenar</param>
        /// <param name="defaultValue">Valor por defecto</param>
        public static void LlenarComboDisplay<T>(ComboBox cmb, ComboBaseItem defaultValue = null) where T : Enum
        {
            var todosLosValores = EnumExtensions.GetEnumListDictionary<T>();
            var lista = todosLosValores.Select(x => new ComboBaseItem() { Value = (Convert.ToInt32(Enum.Parse(typeof(T), x.value.ToString()) as Enum)), Text = x.display });
            Combos.FillCombo(cmb, lista.ToList(), defaultValue);
        }

        /// <summary>
        /// Obtiene la enumeración nullable seleccionada en el combo
        /// </summary>
        /// <typeparam name="T">Enum</typeparam>
        /// <param name="cmb">combo en el que buscar</param>
        /// <returns></returns>
        public static T? GetNullableEnumFromCombo<T>(this ComboBox cmb) where T : struct
        {
            if (((int)cmb.SelectedValue) == 0)
                return null;
            return (T)(object)((int)cmb.SelectedValue);
        }

        /// <summary>
        /// Obtiene la enumeración seleccionada en el combo
        /// </summary>
        /// <typeparam name="T">Enum</typeparam>
        /// <param name="cmb">combo en el que buscar</param>
        /// <returns></returns>
        public static T GetEnumFromCombo<T>(this ComboBox cmb) where T : Enum
        {
            return (T)(object)((int)cmb.SelectedValue);
        }

        /// <summary>
        /// Establece la enumeración indicada como seleccionada en el combo
        /// </summary>
        /// <typeparam name="T">Enum</typeparam>
        /// <param name="cmb">Combo</param>
        /// <param name="value">Nuevo valor para asignar en el combo</param>
        public static void SetEnumFromCombo<T>(this ComboBox cmb, T? value) where T : struct
        {
            if (!value.HasValue)
                cmb.SelectedValue = 0;
            else
                cmb.SelectedValue = (Convert.ToInt32(Enum.Parse(typeof(T), value.Value.ToString()) as Enum));
        }
    }
}
