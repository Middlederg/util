﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace System.Windows.Forms
{
    public static class Mensajes
    {
        public static DialogResult SiNoMensaje(string texto, Form parent = null, string titulo = "Aviso")
         => MessageBox.Show(parent, texto, titulo, MessageBoxButtons.YesNo,
             MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

        public static DialogResult SiNoCancelarMensaje(string texto, Form parent = null, string titulo = "Aviso")
         => MessageBox.Show(parent, texto, titulo, MessageBoxButtons.YesNoCancel,
             MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button3);

        public static DialogResult AceptarCancelarMensaje(string texto, Form parent = null, string titulo = "Aviso")
         => MessageBox.Show(parent, texto, titulo, MessageBoxButtons.OKCancel,
             MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

        public static void Error(string texto, Form parent = null, string titulo = "Error")
        => MessageBox.Show(parent, texto, titulo, MessageBoxButtons.OK, 
            MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

        public static void Aviso(string texto, Form parent = null, string titulo = "Aviso")
        => MessageBox.Show(parent, texto, titulo, MessageBoxButtons.OK,
            MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

        public static void Info(string texto, Form parent = null, string titulo = "Info")
        => MessageBox.Show(parent, texto, titulo, MessageBoxButtons.OK,
            MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);

        public static void UltimoMensaje(this Exception ex, string titulo = "Error", MessageBoxIcon icon = MessageBoxIcon.Error)
        => MessageBox.Show(ex.Mensajes().LastOrDefault() ?? "", titulo, MessageBoxButtons.OK, icon);
        
        public static void TodosLosMensajes(this Exception ex, string titulo = "Error", MessageBoxIcon icon = MessageBoxIcon.Error)
        => MessageBox.Show(string.Join("\n", ex.Mensajes()), titulo, MessageBoxButtons.OK, icon);
    }
}
