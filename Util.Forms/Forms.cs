﻿using System.Collections.Generic;
using System.Collections;

namespace System.Windows.Forms
{
    public static class Forms
    {
        public static DialogResult InformeLvw<T>(this IEnumerable<T> lista, string titulo, Form parent = null,
            int ancho = 400, int alto = 300, bool cabeceras = true, bool stripped = false, ArrayList suma = null )
        {
            ListView lvw = new ListView
            {
                GridLines = true,
                HeaderStyle = cabeceras ? ColumnHeaderStyle.Nonclickable : ColumnHeaderStyle.None,
                FullRowSelect = true,
                MultiSelect = false,
                Dock = DockStyle.Fill,
                View = View.Details
            };
            lista.FillListView(lvw, cabeceras, stripped, null, null, suma);

            Form f = new Form()
            {
                FormBorderStyle = FormBorderStyle.SizableToolWindow,
                Text = titulo,
                StartPosition = FormStartPosition.CenterParent,
                Width = ancho,
                Height = alto
            };
            f.Controls.Add(lvw);
            
            return f.ShowDialog(parent);
        }

        public static Form MaxParent(this Control c)
        {
            if (c.Parent == null)
                return (Form)c;
            return MaxParent(c.Parent);
        }
    }
}
