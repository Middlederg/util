﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util.Forms.Model
{
    /// <summary>
    /// Para usar en los desplegables
    /// </summary>
    public class ComboBaseItem
    {
            /// <summary>
            /// Valor que identifica al item
            /// </summary>
            public int Value { get; set; }

            /// <summary>
            /// Texto que aparece en el combo
            /// </summary>
            public string Text { get; set; }

    }
}
