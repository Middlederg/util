﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Drawing;
using System.Collections;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using Util.Forms.Attributes;

namespace System.Windows.Forms
{
    public static class ListViews
    {
        /// <summary>
        /// llenar listview con lista de strings pasándole las cabeceras
        /// </summary>
        /// <param name="columnas"></param>
        /// <param name="lista"></param>
        /// <param name="lvw"></param>
        public static void LvwRellenar(List<string[]> lista, ListView lvw, IEnumerable<(string nombre, int ancho, HorizontalAlignment align)> columnas)
        {
            lvw.BeginUpdate();
            lvw.Items.Clear();
            lvw.View = View.Details;

            //Agregar cabeceras
            if (lvw.Columns.Count == 0 && columnas != null)
                lvw.Columns.AddRange(GetColumnas(columnas).ToArray());

            //Agregar datos
            foreach (string[] strArray in lista)
            {
                ListViewItem item = new ListViewItem(strArray[0]);
                for (int i = 1; i < strArray.Length; i++)
                    item.SubItems.Add(strArray[i]);
                lvw.Items.Add(item);
            }
            lvw.EndUpdate();
        }

        /// <summary>
        ///  Llenar listview en modo List, con lista de objetos
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lista">Lista de objetos</param>
        /// <param name="lvw">ListView</param>
        /// <param name="imgList">Lista de imágenes</param>
        public static void LvwList<T>(List<T> lista, ListView lvw, ImageList imgList)
        {
            lvw.BeginUpdate();
            lvw.Items.Clear();
            ImageList imgl = new ImageList();
            lvw.View = View.List;
            lvw.SmallImageList = imgList;
            int indx = 0;
            foreach (T elem in lista)
            {
                ListViewItem item = new ListViewItem(elem.ToString())
                {
                    Tag = elem,
                    ImageIndex = indx
                };
                lvw.Items.Add(item);
                indx++;
            }
            lvw.EndUpdate();
        }

        /// <summary>
        /// Llena un ListView con una lista de objetos en el modo detalle
        /// </summary>
        /// <typeparam name="T">Tipo de objeto</typeparam>
        /// <param name="lista">Lista de objetos</param>
        /// <param name="lvw">ListView</param>
        /// <param name="cabeceras">True si se quieren mostrar las cabeceras</param>
        /// <param name="stripped">True si se quiere alternar entre colores</param>
        /// <param name="color1">Color de fondo</param>
        /// <param name="color2">Color para alternar</param>
        /// <param name="suma">Lista de objetos para poner en el pie como totales</param>
        public static void FillListView<T>(this IEnumerable<T> lista, ListView lvw, bool cabeceras, bool stripped = false,
            Color? color1 = null, Color? color2 = null, ArrayList suma = null)
        {
            bool autoresize = false;
            lvw.BeginUpdate();
            color1 = color1 ?? Color.White;
            color2 = color2 ?? Color.LightGray;
            lvw.View = View.Details;
            lvw.Items.Clear();

            //Cabeceras
            if (lvw.Columns.Count == 0)
            {
                var cols = new List<(string nombre, int ancho, HorizontalAlignment align)>();
                foreach (var prop in typeof(T).GetProperties())
                {
                    var customAts = prop.GetCustomAttributes(typeof(TableAttribute), true);
                    if (customAts.FirstOrDefault() is TableAttribute tab)
                        cols.Add((tab.Nombre, tab.Ancho, tab.Align));
                    else
                    {
                        var titulo = prop.DisplayName();
                        cols.Add((string.IsNullOrWhiteSpace(titulo) ? prop.Name : titulo, -2, HorizontalAlignment.Left));
                        autoresize = true;
                    }
                }
                lvw.Columns.AddRange(GetColumnas(cols).ToArray());
            }
            if (!cabeceras) lvw.HeaderStyle = ColumnHeaderStyle.None;

            //Datos
            int iRow = 0;
            foreach (T obj in lista)
            {
                ListViewItem item = new ListViewItem(typeof(T).GetProperties()[0].GetValue(obj, null)?.ToString() ?? "")
                {
                    BackColor = stripped && (iRow % 2 == 0) ? color2.Value : color1.Value,
                    Tag = obj
                };
                for (int i = 1; i < typeof(T).GetProperties().Length; i++)
                    item.SubItems.Add(Contenido(typeof(T).GetProperties()[i], obj));
                lvw.Items.Add(item);
                iRow++;
            }

            //Sumatorios
            if (suma != null && suma.Count > 0)
            {
                ListViewItem item = new ListViewItem(Contenido(suma[0]))
                {
                    BackColor = color1.Value,
                    Tag = null
                };
                for (int i = 1; i < typeof(T).GetProperties().Length; i++)
                {
                    item.SubItems.Add(Contenido(suma[i]));
                }
                lvw.Items.Add(item);
            }

            if (autoresize)
                foreach (ColumnHeader x in lvw.Columns)
                    x.AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);

            lvw.EndUpdate();
        }
        
        /// <summary>
        /// Obtiene la lista de columnas para un ListView
        /// </summary>
        /// <param name="columnas"></param>
        /// <returns></returns>
        private static IEnumerable<ColumnHeader> GetColumnas(IEnumerable<(string nombre, int ancho, HorizontalAlignment align)> columnas)
        {
            foreach (var (nombre, ancho, align) in columnas)
            {
                yield return new ColumnHeader
                {
                    Text = nombre,
                    Width = ancho,
                    TextAlign = align
                };
            }
        }

        /// <summary>
        /// Devuelve el contenido de una propiedad dependiendo de su tipo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        private static string Contenido<T>(PropertyInfo p, T obj)
        {
            if (p.GetValue(obj, null) == null) return "";
            if (p.PropertyType.Equals(typeof(DateTime)) || p.PropertyType.Equals(typeof(DateTime?))) return ((DateTime)p.GetValue(obj, null)).ToShortDateString();
            return p.GetValue(obj, null).ToString();
        }

        /// <summary>
        /// Obtiene el contenido de un objeto
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private static string Contenido(object o)
        {
            if (o == null) return "";
            if (o is DateTime d) return d.ToShortDateString();
            return o.ToString();
        }

        /// <summary>
        /// Devuelve la alineación de una propiedad dependiendo de su tipo
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns></returns>
        private static HorizontalAlignment Alineacion(Type tipo)
        {
            if (tipo.Equals(typeof(int)) || tipo.Equals(typeof(short)) || tipo.Equals(typeof(byte)) || tipo.Equals(typeof(long))) return HorizontalAlignment.Right;
            if (tipo.Equals(typeof(float)) || tipo.Equals(typeof(decimal)) || tipo.Equals(typeof(double))) return HorizontalAlignment.Right;
            if (tipo.Equals(typeof(DateTime))) return HorizontalAlignment.Center;
            return HorizontalAlignment.Left;
        }
    }
}
