﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Util.Forms.Attributes
{
    public class TableAttribute : Attribute
    {
        public string Nombre { get; private set; }
        public int Ancho { get; private set; }
        public HorizontalAlignment Align { get; private set; }

        public TableAttribute(string nombre, HorizontalAlignment align = HorizontalAlignment.Left, int ancho = -2)
        {
            Nombre = nombre;
            Align = align;
            Ancho = ancho;
        }
    }
}
