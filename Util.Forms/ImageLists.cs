﻿using System.Collections.Generic;
using System.Drawing;

namespace System.Windows.Forms
{
    public static class ImageLists
    {
        /// <summary>
        /// Crea una objeto de lista de imagenes a partir de una lista de imágenes
        /// </summary>
        /// <param name="imagenes"></param>
        /// <returns></returns>
        public static ImageList CreateImageList(IEnumerable<Image> imagenes)
        {
            ImageList imgList = new ImageList();
            int i = 0;
            foreach (Image img in imagenes)
                imgList.Images.Add((i++).ToString(), img);
            return imgList;
        }

        /// <summary>
        /// Crea una lista de imagenes a partir de una lista de rutas
        /// </summary>
        /// <param name="imgPaths"></param>
        /// <returns></returns>
        public static ImageList CreateImageList(IEnumerable<string> imgPaths)
        {
            ImageList imgList = new ImageList();
            int i = 0;
            foreach (string path in imgPaths)
                imgList.Images.Add((i++).ToString(), Image.FromFile(path));
            return imgList;
        }

    }
}
