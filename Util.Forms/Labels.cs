﻿using System.Windows.Forms;
using System.Drawing;

namespace System.Windows.Forms
{
    public static class Labels
    {
        public static Label EstadoEnTabla(string nombre, Color colorFondo, Color? colorLetra = null)
        {
            return new Label()
            {
                Text = nombre,
                BackColor = colorFondo,
                ForeColor = colorLetra ?? Color.Black,
                Margin = new Padding(8, 2, 8, 2),
                AutoSize = false
            };
        }
    }
}
