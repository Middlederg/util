﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Util.Core.Test
{
    [TestClass]
    public class DateExtensionsTest
    {
        [TestMethod]
        public void FechaCortaCoincide()
        {
            Assert.AreEqual(new DateTime(2018, 7, 30).FechaCorta(), "30/07/2018");
        }
    }
}
